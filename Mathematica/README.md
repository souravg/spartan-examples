# Mathematica On Spartan

The example test.m is derived from: `https://pages.uoregon.edu/noeckel/Mathematica.html`

It will work as interactive job with Mathematica, step-wise. 

The examples math-simple.m and sample-parallel.m are from `https://rcc.uchicago.edu/docs/software/environments/mathematica/index.html`

X-Windows forwarding will work with Mathematica. It may be necessary to also load the module web_proxy prior to invoking mathematica i.e.,

```
$ ssh username@spartan.hpc.unimelb.edu.au -X
..
$ sinteractive --x11=first
..
$ module load web_proxy
$ module load x11/20190717
$ module load mathematica/12.0.0
$ mathematica
```


# Document Version Control
v1.0 Lev Lafayette, 20200724
